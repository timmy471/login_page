<?php
include 'db.php';
session_start();
if($_SERVER['REQUEST_METHOD']=='GET'){
if( (isset($_GET['email']) && ($_GET['email'] != ''))   AND  (isset($_GET['hash']) && ($_GET['hash'] != '')) ){
   
    $email = $mysqli->real_escape_string($_GET['email']);
    $hash = $mysqli->real_escape_string($_GET['hash']);
    
     //set session variables to use when wanting to update
    $_SESSION['email']= $email;
    $_SESSION['hash']= $hash;

    $result = $mysqli->query("SELECT * FROM user_table WHERE email = '$email' AND hash = '$hash'");
    if ($result->num_rows==0){
        $_SESSION['message'] = 'Invalid details provided';
        header('location:error.php');
    }
}else {
    $_SESSION['message'] = "Sorry, verification failed, try again!";
    header("location: error.php");  
}

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset</title>
</head>
<body>
   <div class = 'container'>
   <h2>Enter your new password</h2>
    <form method = 'POST' action = 'reset_password.php'>
<div class = 'form-group'>
<label for = 'password'>Password:</label>
<input type = 'password' name = 'password1' class = 'form-control' required autocomplete = 'off' placeholder = 'Enter your password'>
</div>

<div class = 'form-group'>
<label for = 'password2'>Please confirm password:</label>
<input type = 'password' name = 'password2' class = 'form-control' required  autocomplete = 'off' placeholder = 'Confirm your password'>
</div>

<div>
<input type = 'submit' name = 'reset' class = 'form-control' value = 'Reset'>
    </form>
   </div> 
</body>
</html>