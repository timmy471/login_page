<?php
include 'db.php';
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = $mysqli->real_escape_string($_POST['email']);

    $result = $mysqli->query("SELECT * FROM user_table WHERE email = '$email'");
    if($result->num_rows==0){
        $_SESSION['message'] = 'user with that email does not exist';
        header('location:error.php');
    }else{
        $row = $result->fetch_array();
        $first_name = $row['first_name'];
        $hash = $row['hash'];
        $email = $row['email'];
        
        $_SESSION['message'] = "<p>Please check your email <span>$email</span>"
        . " for a confirmation link to complete your password reset!</p>";

        $to = $email;
        $header = 'Reset password-Timmy.com';
        $message = "Please follow the link below to reset your password
        http://localhost/tutorial/self/T-Login/reset.php?email='.$email.'&hash='.$hash'
        ";

        //mail($to, $header, $message);

        header('location:success.php');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot</title>
</head>
<body>
    <div class = 'container'>
        <form method = 'POST' action = 'forgot.php'>
            <div class = 'form-group'>
                <label for = 'email'>Email:</label>
                <input type = 'email' class = 'form-control' placeholder = 'Enter your email' name = 'email' autocomplete = 'off'>

            </div>

            <div class = 'form-group'>
                <button type = 'submit' name = 'submit_email'>Submit</button>
            </div>
        </form>
    </div>
</body>
</html>