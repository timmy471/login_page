<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration page</title>
    <link rel = 'stylesheet/css' type = 'css' href = 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>

</head>
<body>
<?php
include 'db.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(isset($_REQUEST['register'])){

    $_SESSION['email'] = $_REQUEST['email'];
    $_SESSION['first_name'] = $_REQUEST['first_name'];
    $_SESSION['last_name'] = $_REQUEST['last_name'];
   

    if ($_REQUEST['password1'] == $_REQUEST['password2']){
        
        $first_name = $mysqli->real_escape_string($_REQUEST['first_name']);
        $last_name = $mysqli->real_escape_string($_REQUEST['last_name']);
        $email = $mysqli->real_escape_string($_REQUEST['email']);
        $password = $mysqli->real_escape_string(password_hash($_REQUEST['password1'], PASSWORD_BCRYPT));
        $hash = $mysqli->real_escape_string(md5(rand(1,1000)));

        $result = $mysqli ->query("SELECT * FROM user_table WHERE email = '$email'");
        if ($result->num_rows>0){
            $_SESSION['message'] = 'email already in use';
            header("location:error.php");
        }else{
            $sql = $mysqli->query("INSERT INTO user_table (first_name, last_name, email, password, hash) VALUES
            ('$first_name', '$last_name', '$email', '$password', '$hash') ");
            
            if($sql){
                $_SESSION['active'] = 0;
                $_SESSION['logged_in'] = true;
                $_SESSION['message'] = "Confirmation link has been sent to $email";

                

                $to = $email;
                $subject = 'Timmy.com;';
                $message = "
        Hello '.$first_name.',

    
        Thank you for signing up!

        Please click this link to activate your account:
        
        http://localhost/tutorial/self/T-Login/verify.php?email='.$email.'&hash='.$hash.'"; 
       // http://localhost/tutorial/self/T-Login/verify.php?email='$email'&hash='$hash; 
       
                
                // $message = "Hello '.$first_name.', 
                // Thank you for signing up. 
                // Please click on the verification link below to activate our account:
                // http://localhost/tutorial/self/T-Login/verify.php?email='.$email.'&hash='.$hash.'";
        
               // $mail_sent = mail($to, $subject, $message);
            //    if ($mail_sent){
            //     $_SESSION['mail_sent'] = 'mail sent';
            //    }else{
            //     $_SESSION['mail_sent'] = 'mail not sent';
            //    }
        
                 header("location:profile.php");
            }
       
        }

        
    }else{
        $_SESSION['message'] = 'passwords do not match';
     header("location:error.php");
    }
} 
}
?>

<div class = 'form'>
 <form action = 'register.php'method = 'POST'>
 

 <div class = 'form-group'>
<label for = 'first_name'>First Name:</label>
<input type = 'text' name = 'first_name' class = 'form-control' required autocomplete = 'off' placeholder = 'Enter your first name'>
</div>

<div class = 'form-group'>
<label for = 'last_name'>Last Name:</label>
<input type = 'text' name = 'last_name' class = 'form-control' required autocomplete = 'off' placeholder = 'Enter your last name'>
</div>

<div class = 'form-group'>
<label for = 'email'>E-mail:</label>
<input type = 'email' name = 'email' class = 'form-control'  required autocomplete = 'off'placeholder = 'Enter your email'>
</div>

<div class = 'form-group'>
<label for = 'password'>Password:</label>
<input type = 'password' name = 'password1' class = 'form-control' required autocomplete = 'off' placeholder = 'Enter your password'>
</div>

<div class = 'form-group'>
<label for = 'password2'>Please confirm password:</label>
<input type = 'password' name = 'password2' class = 'form-control' required  autocomplete = 'off' placeholder = 'Confirm your password'>
</div>

<div>
<input type = 'submit' name = 'register' class = 'form-control' value = 'Submit'>
<p> Already have an account?<button class = 'login-btn'><a href = 'index.php'>LOG IN</a></button></p>
</div>

</form>
</div>
<script src = 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'>
<script src = 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'>

</body>
</html>