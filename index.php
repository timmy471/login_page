<?php
include 'db.php';
session_start();

if($_SERVER['REQUEST_METHOD']=='POST'){
if(isset($_REQUEST['login'])){
    $email = $mysqli->real_escape_string($_REQUEST['email']);
    $password = $mysqli->real_escape_string(strip_tags($_REQUEST['password1']));
    //$_SESSION['email'] = $_REQUEST['email'];
    //$_SESSION['password'] = $_REQUEST['password'];
    $result = $mysqli->query("SELECT * FROM user_table WHERE email='$email'");
    if($result->num_rows == 0){
        $_SESSION['message'] = "There is no user with such details
        please do well to <a href = 'register.php'>register</a>";
        header('location:error.php');
    }else{
        // $row = $result->fetch_object();
        // $db_pass = $row->password;
        //if(password_verify($password, $db_pass)){
        $user = $result->fetch_assoc();

        if ( password_verify($password, $user['password']) ) {
            $_SESSION['logged_in'] = true;
            $_SESSION['message'] = 'passwords match';

            $_SESSION['email'] = $user['email'];
            $_SESSION['first_name'] = $user['first_name'];
            $_SESSION['last_name'] = $user['last_name'];
            $_SESSION['active'] = $user['active'];
            
            
           
             header('location:profile.php');
        }else{
            $_SESSION['message'] = 'wrong password entered';
            //$_SESSION['password'] = $_POST['password1'];
            header('location:error.php');
        }


        // $_SESSION['message'] = "User found";
        // header('location:profile.php');
    }
}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login page</title>
</head>
<body>
    <div class = 'container'>
    <form method = 'POST' action = 'index.php'>
    <div class = 'form-group'>
<label for = 'email'>E-mail:</label>
<input type = 'email' name = 'email' class = 'form-control'  required autocomplete = 'off'placeholder = 'Enter your email'>
</div>

<div class = 'form-group'>
<label for = 'password'>Password:</label>
<input type = 'password' name = 'password1' class = 'form-control' required autocomplete = 'off' placeholder = 'Enter your password'>
</div>

<div>
<input type = 'submit' name = 'login' class = 'form-control' value = 'login'>
<p> No accountt?<button class = 'register-btn'><a href = 'register.php'>Register</a></button></p>
<p> <a href = 'forgot.php'>Forgot password?</a></p>
</div>
    </div>
</body>
</html>